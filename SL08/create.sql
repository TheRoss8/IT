create schema if not exists StruttureRicettive;
use StruttureRicettive;
CREATE TABLE Province
(
  IdProvincia INT AUTO_INCREMENT NOT NULL UNIQUE,
  DenominazioneProvincia VARCHAR(20) NOT NULL,
  PRIMARY KEY (IdProvincia)
);

CREATE TABLE Tipologie
(
  IdTipologia INT AUTO_INCREMENT NOT NULL UNIQUE,
  DenominazioneTipologia VARCHAR(20) NOT NULL,
  PRIMARY KEY (IdTipologia)
);

CREATE TABLE TipologieSecondarie
(
  IdSecondaria INT AUTO_INCREMENT NOT NULL UNIQUE,
  DenominazioneSecondaria INT NOT NULL,
  PRIMARY KEY (IdSecondaria)
);

CREATE TABLE Servizi
(
  IdServizio INT AUTO_INCREMENT NOT NULL UNIQUE,
  DenominazioneServizio VARCHAR(20) NOT NULL,
  PRIMARY KEY (IdServizio)
);

CREATE TABLE Classificazioni
(
  IdCalssificazione INT AUTO_INCREMENT NOT NULL UNIQUE,
  Classificazione VARCHAR(20) NOT NULL,
  PRIMARY KEY (IdCalssificazione)
);

CREATE TABLE Strutture
(
  IdStruttura INT AUTO_INCREMENT NOT NULL UNIQUE,
  Nome VARCHAR(50) NOT NULL,
  Indirizzo VARCHAR(50),
  Numero VARCHAR(50),
  Interno VARCHAR(30),
  CAP VARCHAR(5),
  Telefono VARCHAR(50),
  FAX VARCHAR(30),
  Mail VARCHAR(50),
  Sito VARCHAR(80),
  AltriServizi VARCHAR(50) NOT NULL,
  IdTipologia INT NOT NULL,
  IdSecondaria INT NOT NULL,
  IdProvincia INT NOT NULL,
  IdCalssificazione INT NOT NULL,
  PRIMARY KEY (IdStruttura),
  FOREIGN KEY (IdTipologia) REFERENCES Tipologie(IdTipologia),
  FOREIGN KEY (IdSecondaria) REFERENCES TipologieSecondarie(IdSecondaria),
  FOREIGN KEY (IdProvincia) REFERENCES Province(IdProvincia),
  FOREIGN KEY (IdCalssificazione) REFERENCES Classificazioni(IdCalssificazione)
);

CREATE TABLE Offre
(
  IdStruttura INT NOT NULL,
  IdServizio INT NOT NULL,
  FOREIGN KEY (IdStruttura) REFERENCES Strutture(IdStruttura),
  FOREIGN KEY (IdServizio) REFERENCES Servizi(IdServizio),
  UNIQUE (IdStruttura, IdServizio)
);