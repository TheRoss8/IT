/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jdbctest;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

public class JdbcTest {

	/**
	 * @param args the command line arguments
	 */
	public static void main(String[] args) {
		// TODO code application logic here
		Connection conn = null;
		try {
			// db parameters
			String url = "jdbc:mysql://francescorossato.ddns.net:3306/StruttureRicettive";
			String user = "strutture";
			String password = "Strutture2019";

			// create a connection to the database
			conn = DriverManager.getConnection(url, user, password);
			Statement stmt = conn.createStatement();
			File f = new File("Z:\\SL08\\strutture ricettive veneto\\29833865-0e45-f54d-a17a-93c3920c7e58.csv");
			FileReader fr = new FileReader(f);
			BufferedReader br = new BufferedReader(fr);
			System.out.println(br.readLine());
			String line = br.readLine();
			ArrayList<String> test = new ArrayList();
			String[] in;
			while (line != null) {
				line = line.replace("'", "");
				in = line.split(";");
				if(!test.contains(in[0])){
					test.add(in[0]);
					stmt.execute("insert into `Province`(`DenominazioneProvincia`) values ('" + test.get(test.size()-1) + "');");
				}
				
				
				}
				ResultSet rs = stmt.executeQuery("select * from Province;");
				int size = rs.getFetchSize();
				ArrayList <Integer> ids = new ArrayList();
				ArrayList <String> names = new ArrayList();
				while (rs.next()) {
					ids.add(rs.getInt("IdProvincia"));
					names.add(rs.getString("DenominazioneProvincia"));
					System.out.println(ids.get(ids.size()-1));
					System.out.println(names.get(names.size()-1));
			}
			fr = new FileReader(f);
			br = new BufferedReader(fr);
			System.out.println(br.readLine());
			line = br.readLine();
			while (line != null) {
				in = line.split(";");
				
				for (int i = 0; i < in.length; i++) {
					if (in[i].equals("")) {
						in[i] = "NULL";
					} else {
						in[i] = "'" + in[i] + "'";
					}
				}
				System.out.println(in[0]);
				int pos = 0;
				while(!names.get(pos).equals(in[0]))
					pos++;
				
				int pIndex = ids.get(pos);
				stmt.execute("insert  into `Strutture`(`Nome`,`Indirizzo`,`Numero`,`Interno`,`CAP`,`Telefono`,`FAX`,`Mail`,`Sito`, `IdProvincia`) values " + "(" + in[5] + ", " + in[6] + ", " + in[7] + ", " + in[8] + ", " + in[9] + ", " + in[10] + ", " + in[11] + ", " + in[12] + ", " + in[13] + ", " + pIndex + ") ;");
				
				line = br.readLine();
				
				
        }
		
				
			
			
			
			
			
			
			//stmt.executeBatch();

			
		} catch (IOException e) {
			e.printStackTrace();
		} catch (SQLException ex) {
			Logger.getLogger(JdbcTest.class.getName()).log(Level.SEVERE, null, ex);
		} finally {
			try {
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException ex) {
				System.out.println(ex.getMessage());
			}
		}

	}
}
