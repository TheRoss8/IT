import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.sql.*;

public class SQLoader {
    public static void main(String[] args) {


        File f = new File("Z:\\SL08\\strutture ricettive veneto\\29833865-0e45-f54d-a17a-93c3920c7e58.csv");
        String url = "jdbc:mysql://192.168.56.2:3306/StruttureRicettive";
        String user = "user_strutture";
        String password = "password";

        // create a connection to the database
        try (Connection conn = DriverManager.getConnection(url, user, password);
             Statement stmt  = conn.createStatement();
             ResultSet rs    = stmt.executeQuery("select * from customers")) {
            // loop through the result set
            while (rs.next()) {
                    int coffeeName = rs.getInt(1);
                    String supplierID = rs.getString(2);
                    String price = rs.getString(3);
                    String sales = rs.getString(4);
                    String total = rs.getString(5);
                    System.out.println(coffeeName + "\t" + supplierID +
                            "\t" + price + "\t" + sales +
                            "\t" + total);
                }

            } catch (SQLException e1) {
            e1.printStackTrace();
        }
        }
        try {
            FileReader fr = new FileReader(f);
            BufferedReader br = new BufferedReader(fr);
            System.out.println(br.readLine());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
